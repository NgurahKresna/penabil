<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\DataEmployee;
use App\Models\Precense;
use Illuminate\Http\Request;
use App\Models\Payroll;

class PayrollController extends Controller
{
    //
    public function index(){
        $payroll = Payroll::all();
        return response()->json([
            'http status' => '200',
            'status' => 'true',
            'Message' => 'Success get all data',
            'data' => $payroll
        ], 200);
	   }

    public function countSallary(request $request){
        $id = $request->employee_id;

        $employee = DataEmployee::where('employee_id', $id)->first();
        $employee_category = $employee->category_id;

        $precense = Precense::where('employee_id', $id)->first();
        $total_attandance = $precense->total_attandance;
        $total_overtime = $precense->total_overtime;

        $category = Category::where('category_id', $employee_category)->first();
        $salary_attandance = $category->category_salary;
        $salary_overtime = $category->overtime_salary;

        $total_salary = ($total_attandance * $salary_attandance) + ($total_overtime * $salary_overtime);

        return response()->json([
            'http status' => '200',
            'status' => 'true',
            'Message' => 'Success get all data',
            'total_salary' => $total_salary
        ], 200);

    }

    public function createDataPayroll(request $request){
        $payroll = new Payroll;
        $payroll->employee_id = $request->employee_id;
        $payroll->presence_id = $request->presence_id;
        $payroll->category_id = $request->category_id;
        $payroll->sallary = $request->sallary;
        $payroll->report_date = $request->report_date;
        $payroll->save();

        return response()->json([
            'http status' => '201',
            'status' => 'true',
            'Message' => 'Success insert new employee data',
        ], 201);

    }
}
