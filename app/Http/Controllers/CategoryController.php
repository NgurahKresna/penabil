<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\DataEmployee;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    //
    public function index(){
        $category = Category::all();
        return response()->json([
            'http status' => '200',
            'status' => 'true',
            'Message' => 'Success get all data',
            'data' => $category
        ], 200);
    }

    public function getCategoryById($id){
        $category = Category::where('category_id', $id)->get();
        if(count($category) > 0){ //mengecek apakah data kosong atau tidak
            $res['http status'] = "200";
            $res['status'] = "Success!";
            $res['message'] = "true";
            $res['values'] = $category;
            return response($res);
        }
        else{
            $res['http status'] = "404";
            $res['status'] = "false";
            $res['message'] = "Data not found!";
            return response($res);
        }
    }

    public function createDataCategory(request $request){
        $category = new Category;
        $category->category_name = $request->category_name;
        $category->category_description = $request->category_description;
        $category->category_salary = $request->category_salary;
        $category->overtime_salary = $request->overtime_salary;
        $category->save();

        return response()->json([
            'http status' => '201',
            'status' => 'true',
            'Message' => 'Success insert new employee data',
        ], 201);

    }
}
