<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ClaimReimbursement;

class ClaimReimbursementController extends Controller
{
    //
    public function index(){
        $claimreimbursement = ClaimReimbursement::all();
        return response()->json([
            'http status' => '200',
            'status' => 'true',
            'Message' => 'Success get all data',
            'data' => $claimreimbursement
        ], 200);
}

    public function getClaimReimbursementById($id){
        $claim = ClaimReimbursement::where('claim_id', $id)->get();
        if(count($claim) > 0){ //mengecek apakah data kosong atau tidak
            $res['http status'] = "200";
            $res['status'] = "Success!";
            $res['message'] = "true";
            $res['values'] = $claim;
            return response($res);
        }
        else{
            $res['http status'] = "404";
            $res['status'] = "false";
            $res['message'] = "Data not found!";
            return response($res);
        }
    }


    public function createDataReimbursement(request $request){
        $claimreimbursement = new ClaimReimbursement;
        $claimreimbursement->employee_id = $request->employee_id;
        $claimreimbursement->title = $request->title;
        $claimreimbursement->description = $request->description;
        $claimreimbursement->proff_of_payment = $request->proff_of_payment;
        $claimreimbursement->date = $request->date;
        $claimreimbursement->save();

        return response()->json([
            'http status' => '201',
            'status' => 'true',
            'Message' => 'Success insert new claim reimbursement data',
        ], 201);

    }

}
