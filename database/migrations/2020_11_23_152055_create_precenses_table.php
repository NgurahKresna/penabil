<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrecensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precenses', function (Blueprint $table) {
            $table->increments('presence_id');
            $table->foreignId('employee_id');
            $table->string('total_attandance');
            $table->string('total_absen');
            $table->string('total_overtime');
            $table->date('presence_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precenses');
    }
}
